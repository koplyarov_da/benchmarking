#include <array>
#include <random>
#include <unordered_map>
#include <vector>

#include <stdint.h>

#include <benchmark/benchmark.h>

static std::default_random_engine g_rand((unsigned int)time(0));

static uint8_t random_char()
{ return g_rand() & 0xFF; }


static int switch_lookup(uint8_t c)
{
	switch (c)
	{
	case 0: return 3405668488;
	case 1: return 1056523734;
	case 2: return 1335517863;
	case 3: return 122565260;
	case 4: return 1411976757;
	case 5: return 2182012182;
	case 6: return 1911554224;
	case 7: return 1702603975;
	case 8: return 412880109;
	case 9: return 166342072;
	case 10: return 3086724622;
	case 11: return 3452463155;
	case 12: return 849571693;
	case 13: return 2784726765;
	case 14: return 642787339;
	case 15: return 4194095598;
	case 16: return 2728584357;
	case 17: return 1862023117;
	case 18: return 1230754012;
	case 19: return 3328089982;
	case 20: return 778273279;
	case 21: return 2429782363;
	case 22: return 3641931510;
	case 23: return 248082800;
	case 24: return 3812279668;
	case 25: return 1054810153;
	case 26: return 3526160807;
	case 27: return 2224201744;
	case 28: return 3371392094;
	case 29: return 573560006;
	case 30: return 202726264;
	case 31: return 4102597720;
	case 32: return 1255422183;
	case 33: return 2516082802;
	case 34: return 1183580433;
	case 35: return 768980782;
	case 36: return 3835145267;
	case 37: return 1004073748;
	case 38: return 367130831;
	case 39: return 3947491285;
	case 40: return 2847053700;
	case 41: return 3286340071;
	case 42: return 2155889648;
	case 43: return 703453801;
	case 44: return 2589119584;
	case 45: return 3876911315;
	case 46: return 1743159236;
	case 47: return 1886610035;
	case 48: return 586153936;
	case 49: return 1783485978;
	case 50: return 1521024583;
	case 51: return 3757342038;
	case 52: return 674810540;
	case 53: return 4141458133;
	case 54: return 4095055233;
	case 55: return 1134550311;
	case 56: return 1127661893;
	case 57: return 476145025;
	case 58: return 2078558592;
	case 59: return 1668652719;
	case 60: return 2575307612;
	case 61: return 4222413341;
	case 62: return 11545205;
	case 63: return 402993199;
	case 64: return 1003427708;
	case 65: return 3567178271;
	case 66: return 2728048200;
	case 67: return 3551476584;
	case 68: return 148798684;
	case 69: return 4072120403;
	case 70: return 1799668168;
	case 71: return 1883265734;
	case 72: return 2683506694;
	case 73: return 735621605;
	case 74: return 1703822252;
	case 75: return 2623907518;
	case 76: return 2188998602;
	case 77: return 4072943051;
	case 78: return 3065016776;
	case 79: return 2699599160;
	case 80: return 2662176310;
	case 81: return 2144667166;
	case 82: return 2814929058;
	case 83: return 2047669629;
	case 84: return 1698975709;
	case 85: return 203242330;
	case 86: return 984861377;
	case 87: return 2035049202;
	case 88: return 3408688839;
	case 89: return 731698221;
	case 90: return 3442895819;
	case 91: return 400921240;
	case 92: return 538040492;
	case 93: return 1875037983;
	case 94: return 1269892335;
	case 95: return 668179049;
	case 96: return 3257289945;
	case 97: return 3967165384;
	case 98: return 1260356932;
	case 99: return 1759189664;
	case 100: return 1619189282;
	case 101: return 3220574481;
	case 102: return 2065822625;
	case 103: return 715196738;
	case 104: return 424866031;
	case 105: return 1248525478;
	case 106: return 1540163186;
	case 107: return 4166012619;
	case 108: return 1056400138;
	case 109: return 2982231799;
	case 110: return 4200630494;
	case 111: return 2619710304;
	case 112: return 2825463069;
	case 113: return 61203037;
	case 114: return 2256321045;
	case 115: return 1607498501;
	case 116: return 2159757218;
	case 117: return 701951841;
	case 118: return 4044507119;
	case 119: return 2031401927;
	case 120: return 1891042539;
	case 121: return 3462999129;
	case 122: return 1604835718;
	case 123: return 4122378099;
	case 124: return 532216853;
	case 125: return 95053063;
	case 126: return 1786419932;
	case 127: return 3277297575;
	case 128: return 1364125145;
	case 129: return 1823833352;
	case 130: return 908497126;
	case 131: return 1213020368;
	case 132: return 2631805164;
	case 133: return 291193033;
	case 134: return 697710408;
	case 135: return 1480738275;
	case 136: return 484708473;
	case 137: return 3475897486;
	case 138: return 4170487905;
	case 139: return 2913384030;
	case 140: return 364532737;
	case 141: return 3515574141;
	case 142: return 1754766031;
	case 143: return 1821046174;
	case 144: return 2102178876;
	case 145: return 2387863799;
	case 146: return 212892513;
	case 147: return 1793606494;
	case 148: return 3543948691;
	case 149: return 4018175871;
	case 150: return 1865387245;
	case 151: return 3800264753;
	case 152: return 2839876776;
	case 153: return 3402504622;
	case 154: return 2928679156;
	case 155: return 2614239165;
	case 156: return 1835498879;
	case 157: return 3147547695;
	case 158: return 1572570389;
	case 159: return 840089779;
	case 160: return 2457392287;
	case 161: return 1144918606;
	case 162: return 1408017263;
	case 163: return 4236935502;
	case 164: return 2157227788;
	case 165: return 2457932135;
	case 166: return 1576831374;
	case 167: return 669349611;
	case 168: return 2361249927;
	case 169: return 3534313909;
	case 170: return 849843109;
	case 171: return 1213398027;
	case 172: return 1631758476;
	case 173: return 819837516;
	case 174: return 1578463572;
	case 175: return 324461145;
	case 176: return 1325317457;
	case 177: return 3356754667;
	case 178: return 3664708055;
	case 179: return 3044354545;
	case 180: return 1007399596;
	case 181: return 3655543140;
	case 182: return 2169285871;
	case 183: return 3757211247;
	case 184: return 24839101;
	case 185: return 2610568188;
	case 186: return 2812300650;
	case 187: return 1320403525;
	case 188: return 274162225;
	case 189: return 3091391620;
	case 190: return 473801584;
	case 191: return 3081752022;
	case 192: return 505204399;
	case 193: return 3559968378;
	case 194: return 510269329;
	case 195: return 766386693;
	case 196: return 2229862973;
	case 197: return 2270076892;
	case 198: return 4099247124;
	case 199: return 298831401;
	case 200: return 4277793171;
	case 201: return 1348319915;
	case 202: return 3420091158;
	case 203: return 3263165593;
	case 204: return 1323746203;
	case 205: return 3061193514;
	case 206: return 4057411909;
	case 207: return 330444555;
	case 208: return 2628150775;
	case 209: return 1343402740;
	case 210: return 1481079644;
	case 211: return 2619735051;
	case 212: return 2221502738;
	case 213: return 3422916648;
	case 214: return 1241578526;
	case 215: return 3686365138;
	case 216: return 2588864882;
	case 217: return 3808054499;
	case 218: return 1394253058;
	case 219: return 1904575206;
	case 220: return 1748102711;
	case 221: return 1155811307;
	case 222: return 3227945500;
	case 223: return 1509374789;
	case 224: return 3443999989;
	case 225: return 556985400;
	case 226: return 29630032;
	case 227: return 3917900892;
	case 228: return 2188323303;
	case 229: return 3058267448;
	case 230: return 2686525553;
	case 231: return 9892790;
	case 232: return 386410441;
	case 233: return 422752218;
	case 234: return 3127174291;
	case 235: return 2946240238;
	case 236: return 2155680422;
	case 237: return 3993175162;
	case 238: return 2927047414;
	case 239: return 4153684979;
	case 240: return 660068673;
	case 241: return 136992467;
	case 242: return 1531645664;
	case 243: return 212748620;
	case 244: return 2908124456;
	case 245: return 3088045859;
	case 246: return 4195258086;
	case 247: return 258804084;
	case 248: return 3257633067;
	case 249: return 182965111;
	case 250: return 813038980;
	case 251: return 3089881494;
	case 252: return 345722885;
	case 253: return 3765966695;
	case 254: return 3824485139;
	case 255: return 1491204057;
	default: std::terminate();
	}
}


static void random_char_benchmark(benchmark::State& state)
{
	while (state.KeepRunning())
		benchmark::DoNotOptimize(random_char());
}
BENCHMARK(random_char_benchmark);


static void hash_map_lookup_benchmark(benchmark::State& state)
{
	std::unordered_map<uint8_t, int> m;
	for (int i = 0; i < 256; ++i)
		m[i] = switch_lookup(i);

	while (state.KeepRunning())
		benchmark::DoNotOptimize(m.find(random_char())->second);
}
BENCHMARK(hash_map_lookup_benchmark);


static void vector_lookup_benchmark(benchmark::State& state)
{
	std::vector<int> m(256);
	for (int i = 0; i < 256; ++i)
		m[i] = switch_lookup(i);

	while (state.KeepRunning())
		benchmark::DoNotOptimize(m[random_char()]);
}
BENCHMARK(vector_lookup_benchmark);


static void array_lookup_benchmark(benchmark::State& state)
{
	std::array<int, 256> m;
	for (int i = 0; i < 256; ++i)
		m[i] = switch_lookup(i);

	while (state.KeepRunning())
		benchmark::DoNotOptimize(m[random_char()]);
}
BENCHMARK(array_lookup_benchmark);


static void switch_lookup_benchmark(benchmark::State& state)
{
	while (state.KeepRunning())
		benchmark::DoNotOptimize(switch_lookup(random_char()));
}
BENCHMARK(switch_lookup_benchmark);


BENCHMARK_MAIN()
